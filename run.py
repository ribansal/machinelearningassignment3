import os, sys
path = os.path.join(sys.path[0],'lib')
sys.path.append(path)

import lib
import lib.ICA as ica
import  lib.PCA as pca
import lib.SRP as srp
import lib.LDA as lda
import lib.kmean as kmean
import lib.GMM as gmm
import lib.ANN as ann


def main(method,datatype):
	params = {}
	params['method'] = method
	params['datatype'] = datatype
	params['njobs'] = 4

	if(method == "ICA"):
		ica.run_main(params)
	if(method == "PCA"):
		pca.run_main(params)
	if(method == "SRP"):
		srp.run_main(params)
	if(method == "LDA"):
		lda.run_main(params)
	if(method == "kmeans"):
		kmean.run_main(params)
	if(method == "GMM"):
		gmm.run_main(params)
	if(method == "ANN"):
		ann.run_main(params)




if __name__ == '__main__':
	method = sys.argv[1] #name of method ICA, PCA
	datatype = sys.argv[2] #data mfcc, artificial
	main(method,datatype)
