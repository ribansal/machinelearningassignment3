import numpy as np
from sklearn import metrics



def store_scores(out_scores, X, actual, predicted):
    out_scores["purity_score"]              = purity_score(actual, predicted)
    out_scores["homogeneity_score"]         = metrics.homogeneity_score(actual, predicted)
    out_scores["completeness_score"]        = metrics.completeness_score(actual, predicted)
    out_scores["v_measure_score"]           = metrics.v_measure_score(actual, predicted)
    out_scores["adjusted_rand_score"]       = metrics.adjusted_rand_score(actual, predicted)
    out_scores["adjusted_mutual_info_score"]=          metrics.adjusted_mutual_info_score(actual,  
                                                                                        predicted,
                                                                                        average_method='arithmetic')
    out_scores["silhouette_score"]          = metrics.silhouette_score( X, predicted,
                                                                        metric='euclidean')
    #                                                                    sample_size=sample_size)
    out_scores["calinski_harabasz_score"]   = metrics.calinski_harabasz_score(X, predicted)
    out_scores["davies_bouldin_score"]      = metrics.davies_bouldin_score(X, predicted)

    return out_scores

#https://stackoverflow.com/questions/34047540/python-clustering-purity-metric
def purity_score(y_true, y_pred):
    contingency_matrix = metrics.cluster.contingency_matrix(y_true, y_pred)
    return np.sum(np.amax(contingency_matrix, axis=0)) / np.sum(contingency_matrix) 