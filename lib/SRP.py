import readDataset
import algorithm
import pandas as pd
import numpy as np
import os
import copy

def run_main(params):

    method = params['method']
    datatype = params['datatype']
    params = readDataset.initiate_method(params)
    results = []
    for i in range(5):
    	params['random_state'] = i
    	df = run_srp(params)
    	readDataset.save_log_df_prefix(params,df,'rand_%s'%i)
    	results.append(df)
    algorithm.plot_df_multiple(results,'n_components','error',datatype, 'Number of Components', 'Reconstruction error')

    #run kmeans on transform data
    run_cluster(copy.copy(params))

    run_nn(copy.copy(params))

    #get accuracy vs number of components
    #run_transform_accuracy(copy.copy(params))

    os.chdir("../")

def run_srp(params):

    clfparams = {}
    clfparams['n_components'] = params['nfeature']
    clfparams['dense_output'] = True

    grid = {}
    grid['n_components'] = np.arange(1,params['nfeature']+1)
    keys = ['n_components']   
    space = algorithm.create_arg_combinations(grid,keys)
    params['colname'] = keys + ['density_','error','runtime']
    df = algorithm.run_clf_grid(keys,space,params,clfparams)

    return df


def run_cluster(params):
    
    if params['datatype'] == "artificial":
        n = 5
        params['random_state'] = 2
    elif params['datatype'] == 'avila':
        n = 10
        params['random_state'] = 3
    elif params['datatype'] == 'complex':
        n = 13
        params['random_state'] = 3
    elif params['datatype'] == 'mfcc':
        n = 22
        params['random_state'] = 4

    clfparams = {}
    clfparams['n_components'] = n
    clfparams['dense_output'] = True

    algorithm.run_transform_cluster_algorithm(params,clfparams)

def run_nn(params):
    
    if params['datatype'] == "artificial":
        n = 5
        params['random_state'] = 2
    elif params['datatype'] == 'avila':
        n = 10
        params['random_state'] = 3
    elif params['datatype'] == 'complex':
        n = 13
        params['random_state'] = 3
    elif params['datatype'] == 'mfcc':
        n = 22
        params['random_state'] = 4

    clfparams = {}
    clfparams['n_components'] = n
    clfparams['dense_output'] = True

    algorithm.run_transform_NN_algorithm(params,clfparams)


def run_transform_accuracy(params):

    n = params["nfeature"]
    if params['datatype'] == "artificial":
        params['random_state'] = 2
    elif params['datatype'] == 'avila':
        params['random_state'] = 3
    elif params['datatype'] == 'complex':
        params['random_state'] = 3
    elif params['datatype'] == 'mfcc':
        params['random_state'] = 4

    df = pd.DataFrame()
    for ni in range(1,n+1):
        clfparams = {}
        iterparams = {}
        clfparams['n_components'] = ni
        clfparams['dense_output'] = True
        clfparams['random_state'] = params['random_state']
        iterparams['n_components'] = ni
        iterparams = algorithm.run_transform_NN_accuracy(params,clfparams,iterparams)
        df = df.append(iterparams,ignore_index=True)
    readDataset.save_log_df_name(df,"component_vs_NNaccuracy")