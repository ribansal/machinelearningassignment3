import readDataset
import algorithm
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import copy
import YBplots

def run_main(params):

    method = params['method']
    datatype = params['datatype']
    params = readDataset.initiate_method(params)

    df = run_pca(params)

    df = df.drop(['components_'],axis=1)
    readDataset.save_log_df(params,df)
    
    algorithm.plot_df_col(df,'explained_variance_','n_component','explained_variance_',datatype)
    algorithm.plot_df_col_cumsum(df,'explained_variance_ratio_','Principal components','Explained variance ratio',datatype)
    data = {}
    data['explained_variance_ratio_'] = df['explained_variance_ratio_'].values[0]
    data['explained_variance_'] = df['explained_variance_'].values[0]
    df_data = pd.DataFrame(data)
    df_data.to_csv('%s_variance_vs_ratio.txt'%datatype,sep='\t',float_format='%.4f')
    YBplots.YB_PC_plot(copy.copy(params))
    #run kmeans on transform data
    run_cluster(copy.copy(params))
    run_nn(copy.copy(params))

    #get accuracy vs number of components
    #run_transform_accuracy(copy.copy(params))

    os.chdir("../")


def run_pca(params):

    clfparams = {}
    clfparams['n_components'] = params['nfeature']
    #clfparams['tol'] = 0.0001

    grid = {}
    grid['n_components'] = [params['nfeature']]
    keys = ['n_components']   
    space = algorithm.create_arg_combinations(grid,keys)
    params['colname'] = keys + ['explained_variance_','explained_variance_ratio_','components_' ,'runtime'] 
    df = algorithm.run_clf_grid(keys,space,params,clfparams)

    return df

def run_cluster(params):
    
    if params['datatype'] == "artificial":
        n = 4
    elif params['datatype'] == 'avila':
        n = 8
    elif params['datatype'] == 'complex':
        n = 9
    elif params['datatype'] == 'mfcc':
        n = 16

    clfparams = {}
    clfparams['n_components'] = n

    algorithm.run_transform_cluster_algorithm(params,clfparams)


def run_nn(params):
    
    if params['datatype'] == "artificial":
        n = 4
    elif params['datatype'] == 'avila':
        n = 8
    elif params['datatype'] == 'complex':
        n = 9
    elif params['datatype'] == 'mfcc':
        n = 16

    clfparams = {}
    clfparams['n_components'] = n

    algorithm.run_transform_NN_algorithm(params,clfparams)

def run_transform_accuracy(params):

    n = params["nfeature"]

    df = pd.DataFrame()
    for ni in range(1,n+1):
        clfparams = {}
        iterparams = {}
        clfparams['n_components'] = ni
        iterparams['n_components'] = ni
        iterparams = algorithm.run_transform_NN_accuracy(params,clfparams,iterparams)
        df = df.append(iterparams,ignore_index=True)
    print(df)
    readDataset.save_log_df_name(df,"component_vs_NNaccuracy")













