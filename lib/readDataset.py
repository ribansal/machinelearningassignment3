from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, OneHotEncoder
import numpy as np
import pandas as pd
from sklearn.datasets import make_classification
from sklearn.preprocessing import LabelEncoder

import os, sys

def generate_data_AD_simple():
    np.random.seed(5000)
    X, y = make_classification(n_samples=4000, n_features=6, n_informative=4, 
                                n_redundant=2, n_repeated=0, n_classes=3, 
                                n_clusters_per_class=2, weights=None, flip_y=0.001, 
                                class_sep=10.0, hypercube=True, shift=0.0, scale=1.0, 
                                shuffle=True, random_state=50)

    return X,y

def generate_data_AD():
    np.random.seed(5000)
    X, y = make_classification(n_samples=4000, n_features=5, n_informative=4, 
                                n_redundant=1, n_repeated=0, n_classes=3, 
                                n_clusters_per_class=2, weights=[0.4,0.3,0.3], flip_y=0.05, 
                                class_sep=1.0, hypercube=True, shift=0.0, scale=1.0, 
                                shuffle=True, random_state=50)

    return X,y

def generate_data_AD_complex():
    np.random.seed(5000)
    X, y = make_classification(n_samples=4000, n_features=13, n_informative=8, 
                                n_redundant=2, n_repeated=2, n_classes=6, 
                                n_clusters_per_class=2, flip_y=0.10, 
                                class_sep=2, hypercube=True, shift=0.0, scale=1.0, 
                                shuffle=True, random_state=50)

    return X,y

def generate_data_mfcc():

    np.random.seed(151)
       
    #dataset = pd.read_csv('../pendigits.txt',header=None)
    dataset = pd.read_csv('../Frogs_MFCCs.csv')
    dataset = dataset.drop(['RecordID'],axis=1)
    dataset = dataset.drop(['Family','Species'],axis=1) #Family,Genus,Species

    #preprocessing
    X = dataset.iloc[:, :-1]
    y = dataset.iloc[:, -1]
    label_encoder = LabelEncoder() 
    y = label_encoder.fit_transform(y)
    print(y)
    return X,y

def generate_data_avila():

    np.random.seed(151)
    names = ['intercolumnardistance', 'uppermargin' , 'lowermargin', 'exploitation' , 'rownumber' , 'modularratio' , 'interlinearspacing' , 'weight' , 'peaknumber' , 'modularratiointerlinearspacing', 'class']
    
    dataset = pd.read_csv('../avila-tr.txt', names = names)
    #print(dataset.head())
    #preprocessing
    X = dataset.iloc[:, :-1]
    y = dataset.iloc[:, -1]
    
    lencoder = LabelEncoder()
    y = lencoder.fit_transform(y.values)
    y = pd.DataFrame(y)

    y = y.values.ravel()
    X = X.values 
    return X,y

def preprocess_data(X,y):    
    #preprocess data in training
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1, stratify=y)
    return X_train, X_test, y_train, y_test

def scale_data(X_train, X_test):
    scaler = StandardScaler()
    X_train_scaled = scaler.fit_transform(X_train)
    X_test_scaled = scaler.transform(X_test)
    return X_train_scaled, X_test_scaled

def scale_xdata(X_train):
    scaler = StandardScaler()
    X_train_scaled = scaler.fit_transform(X_train)
    return X_train_scaled


def create_run_dir(method,datatype):
    rundir = "%s_%s_data"%(method,datatype)
    os.system("rm -rf %s && mkdir %s"%(rundir,rundir))
    os.chdir("%s"%rundir)

def preprocess_to_scaledata(X,y):
    X_train, X_test, y_train, y_test = preprocess_data(X,y)
    print('shape of training data is ',X_train.shape)
    X_train, X_test = scale_data(X_train,X_test)
    return X_train, X_test, y_train, y_test

def getunscaleddata(datatype):

    if datatype == "artificial":
        X,y = generate_data_AD()
    elif datatype == 'avila':
        X,y = generate_data_avila()
    elif datatype == 'complex':
        X,y = generate_data_AD_complex()
    elif datatype == 'mfcc':
        X,y = generate_data_mfcc()
    else :
        print("invalid datatype")
        sys.exit()

    return X,y

def getscaleddata(datatype):

    X,y = getunscaleddata(datatype)
    return preprocess_to_scaledata(X,y)

def initiate_method(params):
    method = params['method']
    datatype = params['datatype']
    print("running %s with %s"%(method,datatype))

    #creating run directory
    create_run_dir(method,datatype)

    #get scaled data
    X_train, X_test, y_train, y_test = getscaleddata(datatype)

    params['X_train'] = X_train
    params['X_test'] = X_test
    params['y_train'] = y_train
    params['y_test'] = y_test
    params['nfeature'] = int(X_train.shape[1])
    params['nclass'] = len(np.unique(y_train))

    return params

def set_cluster_size(params):
    if params['datatype'] == "artificial":
        params['maxcluster'] = 51
        params['deltacluster'] = 2
    elif params['datatype'] == 'avila':
        params['maxcluster'] = 101
        params['deltacluster'] = 5
    elif params['datatype'] == 'complex':
        params['maxcluster'] = 30
        params['deltacluster'] = 1
    elif params['datatype'] == 'mfcc':
        params['maxcluster'] = 30
        params['deltacluster'] = 1
    return params


def set_mlpparams(params):
    mlpparams = {}
    if params['datatype'] == "artificial":
        mlpparams['hidden_layer_sizes'] = (19,5,17)
        mlpparams['activation'] = 'relu'
        mlpparams['solver'] = 'lbfgs' 
        mlpparams['alpha'] = 0.1
    elif params['datatype'] == 'avila':
        mlpparams['hidden_layer_sizes'] = (100,70,80)
        mlpparams['activation'] = 'relu'
        mlpparams['solver'] = 'adam' 
        mlpparams['alpha'] = 0.0001
        mlpparams['learning_rate_init'] = 0.01
    elif params['datatype'] == 'complex':
        mlpparams['hidden_layer_sizes'] = (70,75,90)
        mlpparams['activation'] = 'relu'
        mlpparams['solver'] = 'adam' 
        mlpparams['alpha'] = 1e-06
        mlpparams['learning_rate_init'] = 0.01
    elif params['datatype'] == 'mfcc':
        mlpparams['hidden_layer_sizes'] = (30,90,45)
        mlpparams['activation'] = 'relu'
        mlpparams['solver'] = 'lbfgs' 
        mlpparams['alpha'] = 1e-01

    return mlpparams

def set_kmeanparams(params):
    clfparams = {}
    clfparams['max_iter'] = 1000
    clfparams['precompute_distances'] = True
    clfparams['n_jobs'] = 4
    if params['datatype'] == "artificial":
        clfparams['n_clusters'] = 5
    elif params['datatype'] == 'avila':
        clfparams['n_clusters'] = 8
    elif params['datatype'] == 'complex':
        clfparams['n_clusters'] = 11
    elif params['datatype'] == 'mfcc':
        clfparams['n_clusters'] = 11

    return clfparams

def set_gmm_params(params):
    clfparams = {}
    clfparams['max_iter'] = 1000
    clfparams['n_init'] = 2
    if params['datatype'] == "artificial":
        clfparams['n_components'] = 5
    elif params['datatype'] == 'avila':
        clfparams['n_components'] = 5
    elif params['datatype'] == 'complex':
        clfparams['n_components'] = 12
    elif params['datatype'] == 'mfcc':
        clfparams['n_components'] = 17
    else:
        print('invalid datatype')
        sys.exit(0)
    

    return clfparams

def save_log_df(params,df):
    if('isclusteronly' in params):
        filename = "%s_%s_%s"%(params['method'],params['datatype'], params['isclusteronly'])
    else:
        filename = "%s_%s"%(params['method'],params['datatype'])
    df.to_csv(filename+'.csv',sep=",",float_format='%.4f')
    df.to_csv(filename+'.log',sep="\t",float_format='%.4f')

def save_log_df_prefix(params,df,prefix):
    df.to_csv("%s_%s_%s.csv"%(prefix,params['method'],params['datatype']),sep=",",float_format='%.4f')
    df.to_csv("%s_%s_%s.log"%(prefix,params['method'],params['datatype']),sep="\t",float_format='%.4f')

def save_log_df_name(df,name):
    df.to_csv("%s.csv"%(name),sep=",",float_format='%.4f')
    df.to_csv("%s.log"%(name),sep="\t",float_format='%.4f')