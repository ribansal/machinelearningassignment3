import readDataset
import algorithm
import pandas as pd
import numpy as np
import os
import ANN
from sklearn.preprocessing import OneHotEncoder
import copy

def run_main(params):

    method = params['method']
    datatype = params['datatype']
    params = readDataset.initiate_method(params)
    params = readDataset.set_cluster_size(params)

    df = run_kmeans(params)
    post_process_kmean(params,df)

    #run NN on cluster after adding labels
    run_nn_cluster_labels(copy.copy(params))
    run_nn_cluster_only(copy.copy(params))
    os.chdir("../")

def post_process_kmean(params,df):
    datatype = params['datatype']
    prefix = params['method'] + '_' + datatype

    readDataset.save_log_df(params,df)

    metrices = ['runtime','inertia','calinski_harabasz_score','purity_score','homogeneity_score']
    metrices += ['adjusted_rand_score','v_measure_score','silhouette_score','adjusted_mutual_info_score']
    y_label = ['Run time','Distortion','Calinski harabasz score','Purity score','Homogeneity_score'
                ,'Adjusted rand score','V measure score','Silhouette score','Adjusted mutual info score']
    for mi,li in zip(metrices, y_label):
        algorithm.plot_df(df,'n_clusters',mi,prefix, 'Number of clusters',li)
    metrics_combined = ['adjusted_rand_score','v_measure_score','purity_score']
    y_label = ['Adjusted rand score','V measure score','Purity score']
    
    algorithm.plot_df_combined(df,'n_clusters',metrics_combined,prefix, 'Number of clusters',"Score")


def run_kmeans(params):

    clfparams = {}
    clfparams['n_clusters'] = 5
    clfparams['max_iter'] = 1000
    clfparams['precompute_distances'] = True
    clfparams['n_jobs'] = 1

    grid = {}
    grid['n_clusters'] = np.arange(2,params['maxcluster'],params['deltacluster'])
    keys = ['n_clusters']   
    space = algorithm.create_arg_combinations(grid,keys)

    params['colname'] = keys + ['runtime','niter', 'inertia']
    df = algorithm.run_clf_grid(keys,space,params,clfparams)

    return df


def run_kmean_on_transformed_data(params):

    params = readDataset.set_cluster_size(params)
    params['method'] = 'kmeans'
    df = run_kmeans(params)
    post_process_kmean(params,df)

def run_nn_cluster_labels(params):
    clfparams = readDataset.set_kmeanparams(params)
    clf = algorithm.get_classifier(params['method'],clfparams)
    X = params['X_train']
    clf.fit(X)

    Xorig, yorig = readDataset.getunscaleddata(params['datatype'])
    xscale = readDataset.scale_xdata(Xorig)
    ylabels = clf.predict(xscale)
    ohe = OneHotEncoder(sparse=False)
    df1 = pd.DataFrame(Xorig)
    df2 = pd.DataFrame(ylabels)
    df2 = pd.DataFrame(ohe.fit_transform(df2))
    print(df2.shape)
    df = pd.concat([df1,df2],axis=1,ignore_index=False)
    Xnew = df.values

    #split in training and test data for neural network
    X_train, X_test, y_train, y_test = readDataset.preprocess_to_scaledata(Xnew,yorig)

    params['X_train'] = X_train
    params['X_test'] = X_test
    params['y_train'] = y_train
    params['y_test'] = y_test
    params['nfeature'] = int(X_train.shape[1])
    params['nclass'] = len(np.unique(y_train))

    ANN.run_ann_on_transformed_data(params)

def run_nn_cluster_only(params):
    clfparams = readDataset.set_kmeanparams(params)
    clf = algorithm.get_classifier(params['method'],clfparams)
    X_train = params['X_train']
    clf.fit(X_train)


    Xorig, yorig = readDataset.getunscaleddata(params['datatype'])
    xscale = readDataset.scale_xdata(Xorig)
    ylabels = clf.predict(xscale)
    ohe = OneHotEncoder(sparse=False)

    df2 = pd.DataFrame(ylabels)
    df2 = pd.DataFrame(ohe.fit_transform(df2))
    Xnew = df2.values

    #split in training and test data for neural network
    X_train, X_test, y_train, y_test = readDataset.preprocess_to_scaledata(Xnew,yorig)

    params['X_train'] = X_train
    params['X_test'] = X_test
    params['y_train'] = y_train
    params['y_test'] = y_test
    params['nfeature'] = int(X_train.shape[1])
    params['nclass'] = len(np.unique(y_train))
    params['isclusteronly'] = "kmeans_clusteronly"
    ANN.run_ann_on_transformed_data(params)

