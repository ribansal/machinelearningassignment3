import readDataset
import algorithm
import pandas as pd
import numpy as np
import os
import copy


def run_main(params):

    method = params['method']
    datatype = params['datatype']
    params = readDataset.initiate_method(params)
    df = run_ica(params)
    readDataset.save_log_df(params,df)
    algorithm.plot_df(df,'n_components','kurtosis',datatype, 'Components','Kurtosis')

    #run kmeans on transform data
    run_cluster(copy.copy(params))
    run_nn(copy.copy(params))
    #get accuracy vs number of components
    #run_transform_accuracy(copy.copy(params))

    os.chdir("../")



def run_ica(params):

    clfparams = {}
    clfparams['n_components'] = params['nfeature']
    clfparams['max_iter'] = 5000
    clfparams['tol'] = 0.0001

    grid = {}
    grid['n_components'] = np.arange(1,params['nfeature'])
    grid['max_iter'] = [1000]
    keys = ['n_components', 'max_iter']   
    space = algorithm.create_arg_combinations(grid,keys)
    params['colname'] = keys + ['kurt_initial','kurtosis','runtime','niter']
    df = algorithm.run_clf_grid(keys,space,params,clfparams)

    return df

def run_cluster(params):

    if params['datatype'] == "artificial":
        n = 4
    elif params['datatype'] == 'avila':
        n = 8
    elif params['datatype'] == 'complex':
        n = 7
    elif params['datatype'] == 'mfcc':
        n = 10

    clfparams = {}
    clfparams['n_components'] = n
    clfparams['max_iter'] = 5000
    clfparams['tol'] = 0.0001

    algorithm.run_transform_cluster_algorithm(params,clfparams)


def run_nn(params):

    if params['datatype'] == "artificial":
        n = 4
    elif params['datatype'] == 'avila':
        n = 8
    elif params['datatype'] == 'complex':
        n = 7
    elif params['datatype'] == 'mfcc':
        n = 10

    clfparams = {}
    clfparams['n_components'] = n
    clfparams['max_iter'] = 5000
    clfparams['tol'] = 0.0001

    algorithm.run_transform_NN_algorithm(params,clfparams)


def run_transform_accuracy(params):

    n = params["nfeature"]

    df = pd.DataFrame()
    for ni in range(1,n):
        clfparams = {}
        iterparams = {}
        clfparams['n_components'] = ni
        clfparams['max_iter'] = 5000
        clfparams['tol'] = 0.0001
        iterparams['n_components'] = ni
        iterparams = algorithm.run_transform_NN_accuracy(params,clfparams,iterparams)
        df = df.append(iterparams,ignore_index=True)
    print(df)
    readDataset.save_log_df_name(df,"component_vs_NNaccuracy")