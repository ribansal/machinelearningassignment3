import matplotlib.pyplot as plt
from sklearn.metrics import silhouette_samples
from matplotlib import cm
import seaborn as sns
from sklearn.manifold import TSNE


def silhoutte_plot(X, n_clusters, rand_state):
    km = KMeans(n_clusters=n_clusters, random_state=rand_state)
    y_km = km.fit_predict(X)
    cluster_labels = np.unique(y_km)
    n_clusters = cluster_labels.shape[0]
    silhouette_vals = silhouette_samples(X, y_km, metric='euclidean')
    y_ax_lower, y_ax_upper = 0, 0
    yticks = []
    for i, c in enumerate(cluster_labels):
        c_silhouette_vals = silhouette_vals[y_km == c]
        c_silhouette_vals.sort()
        y_ax_upper += len(c_silhouette_vals)
        color = cm.jet(float(i) / n_clusters)
        plt.barh(range(y_ax_lower, y_ax_upper),
                c_silhouette_vals,
                height=1.0, edgecolor='none', color=color)
        yticks.append((y_ax_lower + y_ax_upper) / 2.)
        y_ax_lower += len(c_silhouette_vals)
    silhouette_avg = np.mean(silhouette_vals)
    plt.axvline(silhouette_avg, color="red", linestyle="--")
    plt.yticks(yticks, cluster_labels + 1)
    plt.ylabel('Cluster')
    plt.xlabel('Silhouette coefficient')
    filename = "silhoutte_plot.png"
    plt.savefig(filename, bbox_inches='tight')

def plot_tsna(X,name,y):
    
    tsne = TSNE(n_components=2, random_state=1000)
    X_tsne = tsne.fit_transform(X)
    
    df_subset = pd.DataFrame()
    df_subset['tsne-2d-one'] = X_tsne[:,0]
    df_subset['tsne-2d-two'] = X_tsne[:,1]
    df_subset['y'] = y
    plt.figure(figsize=(16,10))
    sns.scatterplot(
        x="tsne-2d-one", y="tsne-2d-two",
        hue="y",
        #palette=sns.color_palette("hls", 10),
        data=df_subset,
        legend="full"
        #alpha=0.3

    )
    filename = name + "_TSNA_" +  ".png" 
    plt.savefig(filename, bbox_inches='tight')
