from sklearn.decomposition import FastICA
from sklearn.decomposition import PCA
from sklearn.random_projection import SparseRandomProjection
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.cluster import KMeans
from sklearn.mixture import GaussianMixture
from sklearn.neural_network import MLPClassifier
import itertools as it
import pandas as pd
import numpy as np
import time
import copy
import matplotlib.pyplot as plt
import scipy.sparse as sps
from scipy import linalg
import metrics
import kmean
import GMM
import ANN
import readDataset

random_state = 100

def get_classifier(method,params):
	if method == 'ICA':
		params['random_state'] = random_state
		clf = FastICA(**params)
	if method == 'PCA':
		params['random_state'] = random_state
		clf = PCA(**params)
	if method == 'SRP':
		clf = SparseRandomProjection(**params)
	if method == 'LDA':
		clf = LinearDiscriminantAnalysis(**params)
	if method == 'kmeans':
		params['random_state'] = random_state
		clf = KMeans(**params)
	if method == 'GMM':
		params['random_state'] = random_state
		clf = GaussianMixture(**params)
	if method == 'ANN':
		clf = MLPClassifier(**params)

	return clf

def create_arg_combinations(grid,keys):
    space = []
    for gtype in keys:
        space.append(grid[gtype])
    arg_product = list(it.product(*space))
    return arg_product


def run_clf_grid(keys,space,params,clfparams):

	colname = params['colname']
	df = pd.DataFrame(columns=colname)
	X = params['X_train']

	for si in space:
		print('computing grid for ', keys, si)
		for ki in keys:
			clfparams[ki] = si[keys.index(ki)]
		
		itern_start_time = time.perf_counter() 
		clf = get_classifier(params['method'],clfparams)
		if params['method'] == "LDA" or params['method'] == "ANN":
			y = params['y_train']
			clf.fit(X,y)
		else:
			clf.fit(X)
		itern_time = time.perf_counter() - itern_start_time

		iterparams = {}
		for ki in keys:
			iterparams[ki] = si[keys.index(ki)]
		
		iterparams['runtime'] = itern_time
		iterparams = get_method_metrices(clf,params,iterparams)
		df = df.append(iterparams,ignore_index=True)

	return df


def get_method_metrices(clf,params,clfparams):
	
	if params['method'] == "ICA":
		X = params['X_train']
		S = clf.transform(X)
		df = pd.DataFrame(S)
		K = df.kurtosis(axis=0)
		Kmean = K.abs().mean()
		clfparams['kurtosis'] = Kmean
		df = pd.DataFrame(X)
		K = df.kurtosis(axis=0)
		Kmean = K.abs().mean()
		clfparams['kurt_initial'] = Kmean
		clfparams['niter'] =  clf.n_iter_

	if params['method'] == "PCA":
		X = params['X_train']
		#transform data
		pc = clf.transform(X)
		clfparams['explained_variance_'] = clf.explained_variance_
		clfparams['explained_variance_ratio_'] = clf.explained_variance_ratio_
		clfparams['components_'] = clf.components_
		df = pd.DataFrame(pc)
		df.to_csv("pc_%s_%s.csv"%(params['method'],params['datatype']),sep="\t",float_format='%.4f')

	if params['method'] == "SRP":
		X = params['X_train']
		clfparams['density_'] = clf.density_
		error = reconstruction_error(clf,X)
		clfparams['error'] = error
	if params['method'] == "LDA":
		X = params['X_train']
		y = params['y_train']
		clfparams['score'] = clf.score(X,y)
		clfparams['explained_variance_ratio_'] = clf.explained_variance_ratio_
	if params['method'] == "kmeans":
		X = params['X_train']
		clfparams['niter'] =  clf.n_iter_
		clfparams['inertia'] = clf.inertia_
		clfparams = metrics.store_scores(clfparams, X, params['y_train'], clf.labels_)
	if params['method'] == "GMM":
		X = params['X_train']
		ynew = clf.predict(X)
		clfparams['niter'] =  clf.n_iter_
		clfparams['converged_'] = clf.converged_
		clfparams['lower_bound_'] = clf.lower_bound_
		clfparams['AIC'] = clf.aic(X)
		clfparams['BIC'] = clf.bic(X)
		clfparams['score'] = clf.score(X)
		clfparams = metrics.store_scores(clfparams, X, params['y_train'], ynew)
	if params['method'] == "ANN":
		clfparams['trainscore'] = clf.score(params['X_train'],params['y_train'])
		clfparams['testscore']  = clf.score(params['X_test'],params['y_test'])
		print("training accuracy is ",clfparams['trainscore'])
		print("test accuracy is ",clfparams['testscore'])
	return clfparams

def reconstruction_error(proj, x):
    comp = proj.components_
    if sps.issparse(comp):
        comp = comp.todense()
    p_inv = linalg.pinv(comp)
    x_reconstructed = ((p_inv @ comp) @ (x.T)).T
    errors = np.square(x - x_reconstructed)
    return np.nanmean(errors)


def plot_df(df,xcol,ycol,prefix, x_label, y_label):

	fig = plt.figure()
	x = df[xcol].values
	y = df[ycol].values
	plt.grid()
	plt.plot(x, y, linestyle='-', marker='s',markersize=5)    
	plt.xlabel(x_label)
	plt.ylabel(y_label)
	plt.legend(loc='best')
	filename = "%s_%s_%s.png"%(prefix,xcol,ycol)
	plt.savefig(filename,dpi=140)
	plt.close(fig)

def plot_df_combined(df,xcol,ycol_vector,prefix, x_label, y_label):
	fig = plt.figure()
	ax = plt.gca()
	for ycol in ycol_vector:
		df.plot(kind='line',x = xcol,y=ycol,marker='o',ax=ax)

	plt.grid()
	plt.xlabel(x_label)
	plt.ylabel(y_label)
	plt.legend(loc='best')
	#plt.show()
	filename = "%s_%s_%s.png"%(prefix,xcol,y_label)
	plt.savefig(filename, bbox_inches='tight')
	plt.close(fig)
    

def plot_df_multiple(results,xcol,ycol,prefix, x_label, y_label):
    
    fig = plt.figure()
    for i,df in enumerate(results):
	    x = df[xcol].values
	    y = df[ycol].values

	    plt.plot(x, y, linestyle='-', marker='s',markersize=5,label='random_state=%s'%i)

    plt.grid()
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.legend(loc='best')
    filename = "%s_%s_%s.png"%(prefix,xcol,ycol)
    plt.savefig(filename,dpi=140, bbox_inches='tight')
    plt.close(fig)


def plot_df_col(df,col,xlabel,ylabel,prefix):

    fig = plt.figure()
    y = df[col].values[0]
    x = np.arange(1,len(y)+1)

    plt.plot(x, y, linestyle='-', marker='s',markersize=5)

    plt.grid()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    filename = "%s_%s_%s.png"%(prefix,xlabel,ylabel)
    plt.savefig(filename,dpi=140, bbox_inches='tight')
    plt.close(fig)

def plot_df_col_cumsum(df,col,xlabel,ylabel,prefix):

    fig = plt.figure()
    y = df[col].values[0]
    x = np.arange(1,len(y)+1)

    plt.plot(x, np.cumsum(y), linestyle='-', marker='s',markersize=5)

    plt.grid()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    filename = "%s_%s_%s.png"%(prefix,xlabel,ylabel)
    plt.savefig(filename,dpi=140, bbox_inches='tight')
    plt.close(fig)


def run_transform_cluster_algorithm(params,clfparams):
	clf = get_classifier(params['method'],clfparams)
	X = params['X_train']
	if params['method'] == "LDA":
		y = params['y_train']
		clf.fit(X,y)
	else:
		clf.fit(X)	
	xred = clf.transform(X)
	params['X_train'] = xred
	kmean.run_kmean_on_transformed_data(params)
	GMM.run_gmm_on_transformed_data(params)

def run_transform_NN_algorithm(params,clfparams):
	clf = get_classifier(params['method'],clfparams)

	X = params['X_train']
	if params['method'] == "LDA":
		y = params['y_train']
		clf.fit(X,y)
	else:
		clf.fit(X)

	xred = clf.transform(X)
	xtestnew = clf.transform(params['X_test'])
	params['X_train'] = xred
	params['X_test'] = xtestnew
	ANN.run_ann_on_transformed_data(params)


def run_transform_NN_accuracy(params,clfparams,iterparams):

	X = params['X_train']
	
	itern_start_time = time.perf_counter()
	clf = get_classifier(params['method'],clfparams)
	if params['method'] == "LDA":
		y = params['y_train']
		clf.fit(X,y)
	else:
		clf.fit(X)

	xred = clf.transform(X)
	xtestnew = clf.transform(params['X_test'])
	iterparams['classifier_fittime'] = time.perf_counter() - itern_start_time
	
	mlpparams = {}
	mlpparams = readDataset.set_mlpparams(params)
	mlpparams['random_state'] = 10
	mlpparams['max_iter'] = 500
	#mlpparams['early_stopping']=True

	itern_start_time = time.perf_counter() 
	clf = MLPClassifier(**mlpparams)
	clf.fit(xred,params['y_train'])
	iterparams['NN_fittime'] = time.perf_counter() - itern_start_time
	iterparams['n_iter'] = clf.n_iter_
	print("accuracy for iterparams", iterparams)
	iterparams['trainscore'] = clf.score(xred,params['y_train'])
	iterparams['testscore']  = clf.score(xtestnew,params['y_test'])
	print("training accuracy is ",iterparams['trainscore'])
	print("test accuracy is ",iterparams['testscore'])

	return iterparams


