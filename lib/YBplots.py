from yellowbrick.datasets import load_concrete
from yellowbrick.features.pca import PCADecomposition
from yellowbrick.style import set_palette
from sklearn import preprocessing 

def YB_PC_plot(params):

	set_palette('muted')
	filename = "%s_PC_YB.png"%(params['datatype'])
	X = params["X_train"]
	y = params["y_train"]
	label_encoder = preprocessing.LabelEncoder() 
	y= label_encoder.fit_transform(y) 
	visualizer = PCADecomposition(scale=True,proj_features=True, proj_dim=3)
	visualizer.fit_transform(X, y)
	visualizer.show(outpath=filename)