import readDataset
import algorithm
import pandas as pd
import numpy as np
import os

def run_main(params):

    method = params['method']
    datatype = params['datatype']
    params = readDataset.initiate_method(params)
    #params = readDataset.set_mlpparams(params)

    df = run_ann(params)
    post_process_ann(params,df)
    os.chdir("../")

def post_process_ann(params,df):
    datatype = params['datatype']
    readDataset.save_log_df(params,df)

def run_ann(params):

    clfparams = {}
    clfparams = readDataset.set_mlpparams(params)
    clfparams['random_state'] = 10
    clfparams['max_iter'] = 500
    clfparams['early_stopping']=True

    grid = {}
    grid['hidden_layer_sizes'] = [clfparams['hidden_layer_sizes']]
    keys = ['hidden_layer_sizes']   
    space = algorithm.create_arg_combinations(grid,keys)
    params['colname'] = keys + ['runtime','testscore', 'trainscore']
    df = algorithm.run_clf_grid(keys,space,params,clfparams)

    return df
def run_ann_on_transformed_data(params):

    params['method'] = 'ANN'
    df = run_ann(params)
    post_process_ann(params,df)

