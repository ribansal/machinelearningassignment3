import readDataset
import algorithm
import pandas as pd
import numpy as np
import os
import copy

def run_main(params):

    method = params['method']
    datatype = params['datatype']
    params = readDataset.initiate_method(params)
    df = run_lda(params)
    #readDataset.save_log_df(params,df)
    #algorithm.plot_df(df,'n_components','score',datatype)
    algorithm.plot_df_col_cumsum(df,'explained_variance_ratio_','Principal components','Explained variance ratio',datatype)
    data = {}
    data['explained_variance_ratio_'] = df['explained_variance_ratio_'].values[0]
    df_data = pd.DataFrame(data)
    df_data.to_csv('%s_variance_vs_ratio.txt'%datatype,sep='\t',float_format='%.4f')

    #run kmeans on transform data
    run_cluster(copy.copy(params))
    run_nn(copy.copy(params))

    #get accuracy vs number of components
    #run_transform_accuracy(copy.copy(params))

    os.chdir("../")

def run_lda(params):

    clfparams = {}
    clfparams['n_components'] = params['nfeature']
    clfparams['store_covariance'] = True
    max_component = min(params['nfeature'],params['nclass']-1)
    grid = {}
    grid['n_components'] = [max_component]
    keys = ['n_components']   
    space = algorithm.create_arg_combinations(grid,keys)
    params['colname'] = keys + ['score','runtime']
    df = algorithm.run_clf_grid(keys,space,params,clfparams)

    return df

def run_cluster(params):
    
    if params['datatype'] == "artificial":
        n = 2
    elif params['datatype'] == 'avila':
        n = 7
    elif params['datatype'] == 'complex':
        n = 5
    elif params['datatype'] == 'mfcc':
        n = 7

    clfparams = {}
    clfparams['n_components'] = n
    clfparams['store_covariance'] = True

    algorithm.run_transform_cluster_algorithm(params,clfparams)


def run_nn(params):
    
    if params['datatype'] == "artificial":
        n = 2
    elif params['datatype'] == 'avila':
        n = 7
    elif params['datatype'] == 'complex':
        n = 5
    elif params['datatype'] == 'mfcc':
        n = 7

    clfparams = {}
    clfparams['n_components'] = n
    clfparams['store_covariance'] = True

    algorithm.run_transform_NN_algorithm(params,clfparams)

def run_transform_accuracy(params):

    n = min(params['nfeature'],params['nclass']-1)

    df = pd.DataFrame()
    for ni in range(1,n+1):
        clfparams = {}
        iterparams = {}
        clfparams['n_components'] = ni
        clfparams['store_covariance'] = True
        iterparams['n_components'] = ni
        iterparams = algorithm.run_transform_NN_accuracy(params,clfparams,iterparams)
        df = df.append(iterparams,ignore_index=True)
    print(df)
    readDataset.save_log_df_name(df,"component_vs_NNaccuracy")